#!/usr/bin/env python

from display.leds import LED

led = LED()
led.on(1)
led.off(1)
led.on(3)
led.off(3)
led.flash()
led.stopFlashing()
