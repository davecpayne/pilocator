#!/usr/bin/env python

from display.leds import LED
from unlocator.Unlocator import Unlocator
from time import sleep

import sys
import signal
import pifacedigitalio

import logging

logging.basicConfig(filename = '/var/log/pilocator.log', level = logging.DEBUG, format = '%(asctime)s %(levelname)s: %(message)s')
logging.debug('Starting...')

REGION_NAME_NOT_FOUND_ERROR_LED = 0
regionLEDs = {'Canada': 7, 'Colombia': 6, 'UK': 5, 'USA': 4}
regionCodes = {0: 'ca', 1: 'co', 2: 'uk', 3: 'us'}

def setLEDByName(regionName):
	logging.info('Region: ' + str(regionName))
	led.reset()
	if regionName in regionLEDs.keys():
		led.on(regionLEDs[regionName])
	else:
		led.on(REGION_NAME_NOT_FOUND_ERROR_LED)

def setup():
	currentRegion = unl.getRegion()
	setLEDByName(currentRegion)

def buttonPressed(event):
	buttonIndex = event.pin_num
	logging.debug('Button ' + str(buttonIndex) + ' pressed')
	newRegionName = unl.setRegion(regionCodes[buttonIndex])
	setLEDByName(newRegionName)

def exit(signal, frame):
	logging.info('Exiting...')
	led.reset()
	led.on(0)
	listener.deactivate()
	sys.exit(0)


pifacedigital = pifacedigitalio.PiFaceDigital()

led = LED(pifacedigital)
unl = Unlocator()

setup()

listener = pifacedigitalio.InputEventListener(chip = pifacedigital)

for i in range(4):
	listener.register(i, pifacedigitalio.IODIR_FALLING_EDGE, buttonPressed)

listener.activate()

signal.signal(signal.SIGTERM, exit)
signal.signal(signal.SIGINT, exit)
signal.pause()