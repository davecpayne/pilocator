#!/usr/bin/env python

import requests
import logging

from pyquery import PyQuery as pq

class Unlocator:
    def __init__(self):
        self.session = requests.Session()
        self.login()

    def login(self):
        payload = {'amember_login': 'unlocator@davepayne.co.uk', 'amember_pass': 'Common At Feast Stove 9'}
        response = self.session.post('https://unlocator.com/account/login', data=payload)

    def getRegion(self):
        response = self.session.get('https://unlocator.com/account/region-settings')
        logging.debug("response status: " + str(response.status_code))
        logging.debug("get region response: " + response.text)
        region = self.parseRegionSettings(response)

        if (not region):
            if (self.isLoggedOutError(response)):
                self.login()
                return self.getRegion()

        return region

    def setRegion(self, regionCode):
        url = 'https://unlocator.com/tool/api.php?api_key=6a24d020ed8dcf71418cd7853d2b90e6503101e66edfb7ef402c721eb61fda6d&channel=netflix&country=' + regionCode
        logging.debug("setting region with url: " + url)
        response = self.session.get(url)
        logging.debug("response status: " + str(response.status_code))
        logging.debug("set region response: " + response.text)

        return self.getRegion()

    def parseRegionSettings(self, response):
        d = pq(response.text)
        val = d('select[name=netflix] > option[selected]')

        logging.debug("selected option: " + str(val.text()))

        return val.text()

    def isLoggedOutError(self, response):
        d = pq(response.text)
        val = d('div.alert-error')
        if (val.text() == 'You must be authorized to access this area'):
            return True

        return False
