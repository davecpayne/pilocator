#! /usr/bin/env python

import time

class LED:
    def __init__(self, pfd):
        self.pifacedigital = pfd

    def reset(self):
        for i in range(8):
            self.off(i)

    def on(self, pin):
        self.pifacedigital.leds[pin].turn_on()

    def off(self, pin):
        self.pifacedigital.leds[pin].turn_off()
